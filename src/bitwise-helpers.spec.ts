import { BitwiseHelpers } from './bitwise-helpers';
import { expect } from 'chai';

describe('J1939Helpers', () => {

  xdescribe('GetBitAt', ()=>{
    it('returns the value of the bit at the zero-based bitoffset', ()=>{
      expect(BitwiseHelpers.GetBitAt(0x04, 2)).to.equal(true);
      expect(BitwiseHelpers.GetBitAt(0,    5)).to.equal(false);
      expect(BitwiseHelpers.GetBitAt(0xFF, 4)).to.equal(true);
      expect(BitwiseHelpers.GetBitAt(0xF0, 6)).to.equal(true);
      expect(BitwiseHelpers.GetBitAt(0x0F, 6)).to.equal(false);
    });
  });

  xdescribe('SetBitAt', ()=>{
    it('sets the value of the bit at the zero-based bitoffset without affecting other bits', ()=>{
      expect(BitwiseHelpers.SetBitAt(0x04, 2, false)).to.equal(0);
      expect(BitwiseHelpers.SetBitAt(0x04, 2, true)).to.equal(0x04);
      expect(BitwiseHelpers.SetBitAt(0x04, 1, true)).to.equal(0x06);      
      expect(BitwiseHelpers.SetBitAt(0x04, 1, false)).to.equal(0x04);      
    });
  });

});