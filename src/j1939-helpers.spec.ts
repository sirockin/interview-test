import { J1939Helpers } from './j1939-helpers';
import { expect } from 'chai';

describe('J1939Helpers', () => {

  describe('SourceAddressFromCanID', ()=>{
    it('should return the bottom 8 bits  of a 29 bit number ', ()=>{
      
      // Zero example
      expect(J1939Helpers.SourceAddressFromCanID(0)).to.equal(0);

      // Simple example
      expect(J1939Helpers.SourceAddressFromCanID(7)).to.equal(7);

      // Complex example
      expect(J1939Helpers.SourceAddressFromCanID(0x13ABCDEFA7)).to.equal(0xA7);
      

    });
  });


  xdescribe('PGNFromCanID', ()=>{
    it('should return the 18 bits above the first byte, of a 29 bit number ', ()=>{
      
      // Zero example
      expect(J1939Helpers.PriorityFromCanID(0)).to.equal(0);

      // TODO: more test cases
      
    });
  });

  xdescribe('PriorityFromCanID', ()=>{
    it('should return the top 3 bits of a 29 bit number', ()=>{
      
      // Zero example
      expect(J1939Helpers.PriorityFromCanID(0)).to.equal(0);

      // TODO: more test cases

    });
  });

  xdescribe('PriorityFromCanID', ()=>{
    it('should concatenate priority, pgn and sourceAddress into a 29-bit number', ()=>{
      
      // Zero example
      expect(J1939Helpers.PriorityFromCanID(0)).to.equal(0);

      // TODO: more test cases

    });
  });

});