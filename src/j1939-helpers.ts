export class J1939Helpers
{
  // Some helpers to extract PGN, priority and source address from a CANID
  // and to combine them again
  // For more info about the format of a CANID in J1939 look a this page
  // https://www.kvaser.com/about-can/higher-layer-protocols/j1939-introduction/

  // TLDR: A CanID is a 29 bit number which can be split into:
  // - SourceAddress - bottom 8 bits
  // - PGN - next 18 bits
  // - Priority - top 3 bits

  // The methods here should enable us to split J1939 information from a CANID and
  // merge it back again


  static SourceAddressFromCanID( canID: number ) : number
  {
    return canID & 0xFF;
  }

  static PGNFromCanID( canID: number ) : number
  {
    // TODO
    return -1;
  }
  
  static PriorityFromCanID( canID: number ) :number
  {
    // TODO
    return -1;
  }

  static J1939ToCanID( priority:number, pgn:number, sourceAddress:number ) : number
  {
    // TODO
    return -1;
  }
}